from celery import shared_task

from django_datacenter_manager_api.django_datacenter_manager_api.models import (
    DcInstanceApiKey,
)
from django_datacenter_manager_main.django_datacenter_manager_main.models import (
    DcInstance,
)


@shared_task(name="dc_instance_seed_api_keys")
def dc_instance_seed_api_keys():
    dc_instances = DcInstance.objects.all()

    for dc_instance in dc_instances:
        try:
            api_key = DcInstanceApiKey.objects.get(user=dc_instance)
        except DcInstanceApiKey.DoesNotExist:
            api_key = DcInstanceApiKey(user=dc_instance)
            api_key.save()

    return
