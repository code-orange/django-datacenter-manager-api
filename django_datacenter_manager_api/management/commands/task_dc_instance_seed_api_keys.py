from django.core.management.base import BaseCommand

from django_datacenter_manager_api.django_datacenter_manager_api.tasks import (
    dc_instance_seed_api_keys,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        dc_instance_seed_api_keys()
