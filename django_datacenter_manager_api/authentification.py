from tastypie.authentication import *

from django_datacenter_manager_api.django_datacenter_manager_api.models import *


class DcInstanceApiKeyAuthentication(ApiKeyAuthentication):
    auth_type = "apikey"

    def is_authenticated(self, request, **kwargs):
        """
        Finds the user and checks their API key.

        Should return either ``True`` if allowed, ``False`` if not or an
        ``HttpResponse`` if you need something custom.
        """
        try:
            username, api_key = self.extract_credentials(request)
        except ValueError:
            return self._unauthorized()

        if not username or not api_key:
            return self._unauthorized()

        try:
            user = DcInstance.objects.select_related("api_key").get(id=username)
        except (DcInstance.DoesNotExist, DcInstance.MultipleObjectsReturned):
            return self._unauthorized()

        key_auth_check = self.get_key(user, api_key)
        if key_auth_check and not isinstance(key_auth_check, HttpUnauthorized):
            request.user = user

        return key_auth_check

    def get_key(self, user, api_key):
        try:
            if user.api_key.key != api_key:
                return self._unauthorized()
        except DcInstanceApiKey.DoesNotExist:
            return self._unauthorized()

        return True
