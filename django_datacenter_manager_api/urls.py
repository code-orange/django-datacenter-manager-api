from django.urls import path, include
from tastypie.api import Api

from django_datacenter_manager_api.django_datacenter_manager_api.api.resources import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *

v1_api = Api(api_name="v1")
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())
v1_api.register(TunnelResource())
v1_api.register(VlanResource())
v1_api.register(IpSubnetResource())
v1_api.register(IpAddressResource())
v1_api.register(IpNatResource())
v1_api.register(DhcpKeaResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
