from datetime import datetime

from django.db import models
from tastypie.models import ApiKey as TastypieApiKey

from django_datacenter_manager_main.django_datacenter_manager_main.models import (
    DcInstance,
)


class DcInstanceApiKey(TastypieApiKey):
    user = models.OneToOneField(
        DcInstance, related_name="api_key", on_delete=models.CASCADE
    )
    super_admin = models.BooleanField(default=False)
    key = models.CharField(max_length=128, blank=True, default="", db_index=True)
    created = models.DateTimeField(default=datetime.now)

    def create_api_key(sender, instance, created, **kwargs):
        """
        A signal for hooking up automatic ``ApiKey`` creation.
        """
        if kwargs.get("raw", False) is False and created is True:
            DcInstanceApiKey.objects.create(user=instance)

    class Meta:
        abstract = False
        db_table = "dc_instance_api_key"
