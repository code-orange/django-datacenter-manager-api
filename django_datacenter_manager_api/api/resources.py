from django.forms.models import model_to_dict
from tastypie.exceptions import Unauthorized
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_datacenter_manager_api.django_datacenter_manager_api.authentification import (
    DcInstanceApiKeyAuthentication,
)
from django_datacenter_manager_cfg_isc_kea.django_datacenter_manager_cfg_isc_kea.func import *
from django_datacenter_manager_ip.django_datacenter_manager_ip.models import *
from django_datacenter_manager_tun.django_datacenter_manager_tun.models import *
from django_datacenter_manager_vlan.django_datacenter_manager_vlan.models import *


def tunnel_to_dict(tun_connection: DcTunConnection):
    data = model_to_dict(tun_connection)
    data["type"] = model_to_dict(tun_connection.tunnel_type)
    data["options"] = dict()

    for option in tun_connection.dctunoptions_set.all():
        data["options"][option.option] = option.value

    return data


class DatacenterBaseResource(Resource):
    class Meta:
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = DcInstanceApiKeyAuthentication()
        limit = 0
        max_limit = 0

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return


class TunnelResource(DatacenterBaseResource):
    def obj_get(self, bundle, **kwargs):
        cmdb_host = str(kwargs["pk"])

        bundle.obj = DcTunConnection.objects.filter(cmdb_host=cmdb_host)

        return bundle

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = dict()

            for tun_connection in bundle.obj.obj:
                bundle.data[tun_connection.pk] = tunnel_to_dict(tun_connection)
                bundle.data[tun_connection.pk]["clients"] = dict()

                for tun_client in DcTunConnection.objects.filter(
                    related_to=tun_connection
                ):
                    bundle.data[tun_connection.pk]["clients"][tun_client.pk] = (
                        tunnel_to_dict(tun_client)
                    )

        return bundle


class VlanResource(DatacenterBaseResource):
    def obj_get(self, bundle, **kwargs):
        vlan_id = int(kwargs["pk"])

        dc_vlan = DcVlan.objects.get(id=vlan_id)

        if bundle.request.user.api_key.super_admin is not True:
            if not dc_vlan.instance_id == bundle.request.user.id:
                raise Unauthorized

        return dc_vlan

    def obj_get_list(self, bundle, **kwargs):
        vlan_list = DcVlan.objects.filter(instance=bundle.request.user.id)
        return vlan_list

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)

        bundle.data["networks"] = list()

        for network in bundle.obj.dcipnetwork_set.all():
            bundle.data["networks"].append(model_to_dict(network))

        return bundle


class IpSubnetResource(DatacenterBaseResource):
    def obj_get(self, bundle, **kwargs):
        subnet_id = int(kwargs["pk"])

        dc_ip_network = DcIpNetwork.objects.get(id=subnet_id)

        if bundle.request.user.api_key.super_admin is not True:
            if not dc_ip_network.vlan.instance_id == bundle.request.user.id:
                raise Unauthorized

        return dc_ip_network

    def obj_get_list(self, bundle, **kwargs):
        network_list = DcIpNetwork.objects.filter(vlan__instance=bundle.request.user.id)
        return network_list

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)

        bundle.data["reservations"] = list()

        for reservation in bundle.obj.dcipaddress_set.exclude(
            mac_address="ff:ff:ff:ff:ff:ff"
        ):
            bundle.data["reservations"].append(model_to_dict(reservation))

        return bundle


class IpAddressResource(DatacenterBaseResource):
    class Meta(DatacenterBaseResource.Meta):
        allowed_methods = ["get", "post"]

    def obj_get(self, bundle, **kwargs):
        ip_address_id = int(kwargs["pk"])

        dc_ip_address = DcIpAddress.objects.get(id=ip_address_id)

        if bundle.request.user.api_key.super_admin is not True:
            if not dc_ip_address.network.vlan.instance_id == bundle.request.user.id:
                raise Unauthorized

        return dc_ip_address

    def obj_get_list(self, bundle, **kwargs):
        ip_list = DcIpAddress.objects.filter(
            network__vlan__instance=bundle.request.user.id
        ).exclude(mac_address="ff:ff:ff:ff:ff:ff")

        return ip_list

    def obj_create(self, bundle, **kwargs):
        network_id = bundle.data["network_id"]
        ip_address = bundle.data["ip_address"]

        dc_ip_address = DcIpAddress.objects.get(
            network__vlan__instance=bundle.request.user.id,
            network_id=network_id,
            ip_address=ip_address,
        )

        if "mac_address" in bundle.data:
            dc_ip_address.mac_address = bundle.data["mac_address"]

        if "reverse_fqdn" in bundle.data:
            dc_ip_address.reverse_fqdn = bundle.data["reverse_fqdn"].replace("_", "-")

        dc_ip_address.save()

        bundle.obj = dc_ip_address

        return bundle

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)
        return bundle


class IpNatResource(DatacenterBaseResource):
    class Meta(DatacenterBaseResource.Meta):
        allowed_methods = ["get"]

    def obj_get(self, bundle, **kwargs):
        ip_nat_id = int(kwargs["pk"])

        dc_ip_nat = DcIpNatMasquerade.objects.get(id=ip_nat_id)

        if bundle.request.user.api_key.super_admin is not True:
            if not dc_ip_nat.network.vlan.instance_id == bundle.request.user.id:
                raise Unauthorized

        return dc_ip_nat

    def obj_get_list(self, bundle, **kwargs):
        ip_nat_list = DcIpNatMasquerade.objects.filter(
            network__vlan__instance=bundle.request.user.id
        )

        return ip_nat_list

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)
        bundle.data["network"] = model_to_dict(bundle.obj.network)
        bundle.data["ip_address"] = model_to_dict(bundle.obj.ip_address)
        return bundle


class DhcpKeaResource(DatacenterBaseResource):
    def obj_get(self, bundle, **kwargs):
        instance_id = int(kwargs["pk"])

        if bundle.request.user.api_key.super_admin is not True:
            if not instance_id == bundle.request.user.id:
                raise Unauthorized

        dc_instance = DcInstance.objects.get(id=instance_id)

        return dc_instance

    def dehydrate(self, bundle):
        bundle.data = dict()

        only_network_ids = None

        if "only_network_ids" in bundle.request.GET:
            only_network_ids = (
                str(bundle.request.GET["only_network_ids"]).replace(" ", "").split(",")
            )

        bundle.data["dhcp4"] = generate_dhcp4_config(bundle.obj, only_network_ids)
        bundle.data["dhcp6"] = generate_dhcp6_config(bundle.obj, only_network_ids)

        return bundle
